# NDMS DevOps Exercise - Containerize a Java Rest Microservice (SpringBoot) using Docker

In this excerise you will need to containizer the NDMS Hello World Service and implement the necessary scripts to build, deploy, and monitor the health of this service.

---

## TL;DR *(Too Long;Didn't Read)* Instructions
1. Clone NDMS hello world service repository.
2. Run the service locally using Gradle (using supplied archive or building form source) to verify functionality.
3. Create your own Dockerfile to build an image for the service.
4. Automate the monitoring of the health of the service. 

The rest of this document explains, in detail, what is expected in this exercise and how to do it.  **Be sure to check specific sections for requirements** *(e.g., setting the exposed port to 8091 in the image)*.

---

## Cloning the NDMS Hello Repository

You'll start by making sure the version control system Git is installed on your system.  If it is not installed, please go to the [offcial Git website](https://git-scm.com/downloads) to download the installation binary.  When Git is installed and working, please proceed to setting up your local repository with the steps below.

1. Go to the [NDMS Hello World Service BitBucket Cloud Repository](https://bitbucket.org/ndms-devops-group/hello-world-service/src/master/) and select **Clone** on the top right of the page.
2. **Copy the URL** to clone or **clone using Sourcetree** (if you used it).
3. **Open a new Git Bash session** (or terminal of your choice) and navigate to the folder where you'd like to clone this repository.
4. **Clone the repository**
5. Open the directory you just created to see your repository’s files.

**Cloning the NDMS Hello World Service using the shell**:
```shell
$> mkdir -p my/new/repository
$> cd my/new/repository
$> git clone https://<user_name>@bitbucket.org/ndms-devops-group/hello-world-service.git
```

---

## Running the NDMS Hello World Service via Gradle
Running the service locally is easy!  In the directory where your repository was cloned, open up your favorite terminal application (Git Bash, Windows Command Shell, PowerShell, etc) and **run the following in the root of the repository** (where all the gradle files are):
```shell
$> gradle bootRun
```
This will tell gradle to start the Hello service using SpringBoot locally (via an embedded Tomcat container).  You can then use your browser to access the service locally.  Give it a try!  

**In your browser access the following URL:**
```
https://localhost:8091/rest/v1/hello
```
The service will greet you with a warming message:
> *"Welcome to NDMS, citizen"*.

---

## Create a Dockerfile for the NDMS Hello Service

Next, create a Dockerfile to containerize the NDMS Hello Service.

**Requirements**

- **You may use any base Docker image you want in order to build your image.**  We have provided an Alpine 3.10 base image on [Docker Hub](https://hub.docker.com/repository/docker/cdempsey1221/alpine-gradle) for your convenience with the following preinstalled: Amazon Coretto 11 JDK and Gradle 5.6.1 build system.  To pull the provided image:
```shell
$> docker pull cdempsey1221/alpine-gradle:5.6.1-jdk11
```

- **You may use the prebuilt jar provided in the packaged zip archive** in the **hello-world-service/dist** directory or **build a new jar directly from source using Gradle** (build file is in the root of the repository).  See the bottom of this section to build the Hello service from Source.
- The Hello World Service **must be exposed on port 8091**.
- The image must **start the application** (SpringBoot) when run.
- Script(s) need to be provided to both **build and run the NDMS Hello service** image.  

**You may use any start/bootstrap script(s) you want in any language you prefer.**  The most common languages we use for this task are:
- Bash Shell
- Python
- Ruby  

You may also use a provisioning tool such as Ansible, Chef, or Puppet in order to build and deploy the NDMS Hello World Service.

Now that you're more familiar with the NDMS Hello Service Bitbucket repository, go ahead and start making changes locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

### **Building the Hello World Service from Source Using Gradle**
```shell
$> cd <cloned_repository>/hello-world-service  # change directory to your cloned local NDMS Hello World Service directory
$> gradle bootJar  # build the project and create an executable fat jar
$> gradle packageDistribution  # zip it all up and put it into the /dist directory of the project
```
---

## NDMS Hello Rest Microservice API
The service provides the following API calls:

### **Hello World API**
 - **Service Method:  index**
 - URL:  http://localhost:8091/rest/v1/hello
 - HTTP Method:  GET
 - Input Paramaters: none
 - Output:  String
    - Value:  "Welcome to NDMS, citzien"

- **Service Method:  name**
 - URL:  http://localhost:8091/rest/v1/hello/${name}
 - HTTP Method:  GET
 - Input Paramaters: String
 - Output:  String
    - Value:  "It's nice to meet you, ${name}"

### **Service Health Check**
- Service Method:  health
 - URL:  http://localhost:8091/actuator/health
 - HTTP Method:  GET
 - Input Paramaters: none
 - Output:  Json (message indicating if service is up or down)
```json
{
    "status": "UP"
}
```

You can use the health check to verify that the NDMS Hello Service was deployed and is running properly.

---

## Image Testing & Verification

Lastly, verify the NDMS Hello Service is operational by implementing a script that can monitor it's health.  This can be done in many ways such as:

- Docker Healthcheck
- Bash shell script
- Python script
- Ansible playbook

Implement this in any way you choose.  Please read the requirements below.

**Requirements**

- Must be implemented in a way that can be automatically executed from a CI/CD environment (such as a Docker Healthcheck or script).
- Verification must make a REST call to the service using the health check endpoint to verify it's running.  *See the section above that describes the service health check.*

---

## Create a Feature Branch and Push to Origin

Once the exercise is done, create a new feature branch with the the following format:  **feature/my-branch-name**.

Commit all your changes and push the branch to the Bitbucket [origin] (https://bitbucket.org/ndms-devops-group/hello-world-service/src/master/) remote repository.  You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

When you've pushed your feature branch to the origin, **create a new pull request** with the following:
- source branch:  **the feature branch you pushed** (e.g., feature/my-branch-name).
- target branch:  **master**
- title:  summarize your changes for this feature
- description:  high level description of all changes that are a part of this PR
- reviewers:  Parker Garrison & Chris Dempsey

Thank you for your interest in working with us at NDMS!

### **Made by the NDMS DevOps Team**
```json
{
    "devops": {
        "members": {
        "parker": {"name": "Parker Garrison", "position": "Lead DevOps Engineer" },
        "chris": {"name": "Chris Dempsey", "position": "DevOps Engineer" }
        }
    }
}
```