package mil.navy.navair.ndms.devops.hello.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mil.navy.navair.ndms.devops.hello.NameNotProvidedException;

@ControllerAdvice
public class NameNotProvidedAdvice {
	
	@ResponseBody
	@ExceptionHandler(NameNotProvidedException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	private String nameNotProvidedHandler(NameNotProvidedException ex) {
		return (ex.getMessage());
	}

}
