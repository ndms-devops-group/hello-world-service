package mil.navy.navair.ndms.devops.hello;

public class NameNotProvidedException extends RuntimeException {

	private static final long serialVersionUID = -6342509438340606023L;
	
	public NameNotProvidedException() {
		super("Name must not be blank or empty to greet");
	}

}
