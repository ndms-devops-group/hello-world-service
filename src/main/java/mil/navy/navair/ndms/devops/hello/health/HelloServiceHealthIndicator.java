package mil.navy.navair.ndms.devops.hello.health;

import java.net.InetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class HelloServiceHealthIndicator implements HealthIndicator {
	
	protected static final String HELLO_SERVICE_KEY = "NDMS Hello Service";
	
	private RestTemplate template = new RestTemplate();
	
	@Autowired
	private Environment environment;
	
	@Override
	public Health health() {
		
		Health healthResult = null;
		
		if (!(isHelloServiceRunning())) {
			healthResult = Health
					.down()
					.withDetail(HELLO_SERVICE_KEY, "Not Available")
					.build();
		} else {
			healthResult = Health
					.up()
					.withDetail(HELLO_SERVICE_KEY, "UP")
					.build();
		}
		
		return (healthResult);
	}
	
	private boolean isHelloServiceRunning() {
		final String helloServiceUrl = "/rest/v1/hello";
		boolean retVal = true;
		
		ResponseEntity<String> response = 
				template.getForEntity(getOriginUrl() + "/" + helloServiceUrl, String.class);
		
		if (!(response.getStatusCode().is2xxSuccessful()) 
				&& !(response.getBody().equalsIgnoreCase("Welcome to NDMS, citizen"))) {
			retVal = false;
		}
		
		return (retVal);
	}
	
	private String getOriginUrl() {
		String runningPort = environment.getProperty("server.port");
		String hostName = null;
		
		try {
			hostName = InetAddress.getLoopbackAddress().getHostName();
		} catch (Exception ex) {
			// default to LocalHost
			hostName = "localhost";
		}
		
		return (new StringBuilder()
					.append("http://")
					.append(hostName)
					.append(":")
					.append(runningPort)
					.toString() );
	}
}
