package mil.navy.navair.ndms.devops.hello.service;

public interface HelloService {

	String defaultGreeting();

	String greetingWithName(String name);

}