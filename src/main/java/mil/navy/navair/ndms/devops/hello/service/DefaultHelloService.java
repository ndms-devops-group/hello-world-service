package mil.navy.navair.ndms.devops.hello.service;


import org.springframework.stereotype.Service;

import com.google.common.base.Strings;

import mil.navy.navair.ndms.devops.hello.NameNotProvidedException;

@Service
public class DefaultHelloService implements HelloService {
	
	@Override
	public String defaultGreeting() {
		return( "Welcome to NDMS, citizen");
	}
	
	@Override
	public String greetingWithName(String name) {
		final String providedName = Strings.nullToEmpty(name);
		final String greetFmtString = "It's nice to meet you, %s";
		if ( !(providedName.isBlank()) ) {
		} else {
			throw new NameNotProvidedException();
		}
		return( String.format(greetFmtString, name) );
	}
}