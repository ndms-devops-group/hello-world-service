package mil.navy.navair.ndms.devops.hello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mil.navy.navair.ndms.devops.hello.service.HelloService;

@RestController
@RequestMapping("/rest/v1")
public class HelloController {
	
	@Autowired
	private HelloService helloService;
	
	@GetMapping("/hello")
	public String index() {
		return ( helloService.defaultGreeting() );
	}
	
	@GetMapping("/hello/{name}")
	public String name(@PathVariable String name) {
		return ( helloService.greetingWithName(name) );
	}
}
