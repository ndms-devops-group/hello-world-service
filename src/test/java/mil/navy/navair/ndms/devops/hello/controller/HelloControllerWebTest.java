package mil.navy.navair.ndms.devops.hello.controller;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HelloControllerWebTest {
	
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void shouldReturnDefaultGreeting() {
		
		final String callUrl = "hello";
		
		ResponseEntity<String> response = restTemplate.getForEntity(buildServiceUrl(callUrl), String.class);
		
		assertTrue(response.getStatusCode().is2xxSuccessful());
		assertTrue(response.getBody().equalsIgnoreCase("Welcome to NDMS, citizen"));
	}
	
	@Test
	public void shouldGreetWithGivenName() {
		
		final String callUrlFmt = "hello/%s";
		final String urlWithName = String.format(callUrlFmt, "Tyrion");
		
		ResponseEntity<String> response = restTemplate.getForEntity(buildServiceUrl(urlWithName), String.class);
		
		assertTrue(response.getStatusCode().is2xxSuccessful());
		assertTrue(response.getBody().equalsIgnoreCase("It's nice to meet you, Tyrion"));
	}
	
	private String buildServiceUrl(String serviceMethodCall) {
		final String originUrl = buildOriginUrl();
		final String serviceUrl = "rest/v1";
		
		return (new StringBuilder()
				.append(originUrl)
				.append("/")
				.append(serviceUrl)
				.append("/")
				.append(serviceMethodCall).toString());
	}
	
	private String buildOriginUrl() {
		
		final String protocol = "http://";
		
		return (new StringBuilder()
					.append(protocol)
					.append("localhost")
					.append(":")
					.append(port)
					.toString() );
	}

}
