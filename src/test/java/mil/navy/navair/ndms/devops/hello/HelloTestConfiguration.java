package mil.navy.navair.ndms.devops.hello;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "mil.navy.navair.ndms.devops")
public class HelloTestConfiguration {}
