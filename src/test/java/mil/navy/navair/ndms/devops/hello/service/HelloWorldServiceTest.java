package mil.navy.navair.ndms.devops.hello.service;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import mil.navy.navair.ndms.devops.hello.HelloTestConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes= {HelloTestConfiguration.class})
public class HelloWorldServiceTest {
	
	@Autowired
	private HelloService helloService;
	
	@Test
	public void verifyDefaultMsg() {
		assertNotNull(this.helloService);
		assertTrue("Welcome to NDMS, citizen"
				.equalsIgnoreCase(helloService.defaultGreeting()));
	}
	
	public void verifyGreetingIsCorrect() {
		assertNotNull(this.helloService);
		assertTrue("It's nice to meet you, chris"
				.equalsIgnoreCase(this.helloService.greetingWithName("chris")));
	}

}
